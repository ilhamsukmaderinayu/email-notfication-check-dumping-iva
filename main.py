import json
import psycopg2
import os
from datetime import datetime, timedelta
import smtplib
import time
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders

config_file = "/usr/local/email-notfication-check-dumping-iva/config.json"

data_cfg = json.load(open(config_file))
dump_api = data_cfg['api_url']
db_address = data_cfg['db_address']
db_username = data_cfg['db_username']
db_password = data_cfg['db_password']
db_port = data_cfg['db_port']
db_name = data_cfg['db_name']

def inactive_trx_id(inactive_cam_id, trx_list):
	trx_dead = []
	for dead in inactive_cam_id:
		for data in trx_list:
			# print(data)
			if data['camera_id'] == dead:
				trx_dead.append(data)
	return trx_dead

def inactive_camera_id(cam_id_trx_list, cam_id_dumping_list):
    cam_diff = list(set(cam_id_trx_list) - set(cam_id_dumping_list))
    return cam_diff

def create_body(used_rule,date_report,hour_report,inactive_transaction_ce,inactive_transaction_fr_known,inactive_transaction_fr_unknown,inactive_transaction_vc,inactive_transaction_lpr,inactive_transaction_pc,inactive_transaction_cb,date_check):
	body = "Berikut kami lampirkan hasil monitoring IVA Polda Jawa Timur: "+"\n"+"Tanggal : " + str(date_check)+"\n"+"Jam : "+str(hour_report)+".00 WIB"+"\n"
	missing_cam_ce=""
	missing_cam_detail_ce=""
	missing_cam_fr_known=""
	missing_cam_detail_fr_known=""
	missing_cam_fr_unknown=""
	missing_cam_detail_fr_unknown=""
	missing_cam_vc = ""
	missing_cam_detail_vc=""
	missing_cam_lpr = ""
	missing_cam_detail_lpr=""
	missing_cam_pc = ""
	missing_cam_detail_pc=""
	missing_cam_cb=""
	missing_cam_detail_cb=""

	if len(inactive_transaction_ce)==0 and "Crowd Estimation" in used_rule:
		missing_cam_ce = ">>>> Terdapat "+str(len(inactive_transaction_ce))+" camera CROWD ESTIMATION tidak melakukan dumping"+"\n"
		missing_cam_detail_ce= ""
	else:
		for i in used_rule:
			for data in inactive_transaction_ce:
				if data["rule_name"] in used_rule:
					missing_cam_ce = ">>>> Terdapat "+str(len(inactive_transaction_ce))+" camera CROWD ESTIMATION tidak melakukan dumping"+"\n"
					missing_cam_detail_ce= ""
					count = 1
					for data in inactive_transaction_ce:
						missing_cam_detail_ce = missing_cam_detail_ce  +str(count) + ". camera id "+str(data['camera_id']) +" dengan transaksi : "+ str(data['trx_id'])+ "\n"
						count=count+1

	if len(inactive_transaction_fr_known)==0 and "Face Recognition" in used_rule:
		missing_cam_fr_known = ">>>> Terdapat "+str(len(inactive_transaction_fr_known))+" camera FR-KNOWN tidak melakukan dumping"+"\n"
		missing_cam_detail_fr_known= ""
	else:
		for data in inactive_transaction_fr_known:
			if data["rule_name"] in used_rule:
				missing_cam_fr_known = ">>>> Terdapat "+str(len(inactive_transaction_fr_known))+" camera FR-KNOWN tidak melakukan dumping"+"\n"
				missing_cam_detail_fr_known= ""
				count = 1
				for data in inactive_transaction_fr_known:
					missing_cam_detail_fr_known = missing_cam_detail_fr_known  +str(count) + ". camera id "+str(data['camera_id']) +" dengan transaksi : "+ str(data['trx_id'])+ "\n"
					count=count+1

	if len(inactive_transaction_fr_unknown)==0 and "Face Recognition" in used_rule:
		missing_cam_fr_unknown = ">>>> Terdapat "+str(len(inactive_transaction_fr_unknown))+" camera FR-UNKNOWN tidak melakukan dumping"+"\n"
		missing_cam_detail_fr_unknown=""
	else:
		for data in inactive_transaction_fr_unknown:
			if data["rule_name"] in used_rule:
				missing_cam_fr_unknown = ">>>>  Terdapat "+str(len(inactive_transaction_fr_unknown))+" camera FR-UNKNOWN tidak melakukan dumping"+"\n"
				missing_cam_detail_fr_unknown= ""
				count = 1
				for data in inactive_transaction_fr_unknown:
					missing_cam_detail_fr_unknown = missing_cam_detail_fr_unknown  +str(count) + ". camera id "+str(data['camera_id']) +" dengan transaksi : "+ str(data['trx_id'])+ "\n"
					count=count+1

	if len(inactive_transaction_vc)==0 and "Vehicle Counting" in used_rule:
		missing_cam_vc= ">>>> Terdapat "+str(len(inactive_transaction_vc))+" camera VEHICLE COUNTING tidak melakukan dumping"+"\n"
		missing_cam_detail_vc= ""
	else:
		for data in inactive_transaction_vc:
			if data["rule_name"] in used_rule:
				missing_cam_vc= ">>>> Terdapat "+str(len(inactive_transaction_vc))+" camera VEHICLE COUNTING tidak melakukan dumping"+"\n"
				missing_cam_detail_vc= ""
				count = 1
				for data in inactive_transaction_vc:
					missing_cam_detail_vc = missing_cam_detail_vc  +str(count) + ". camera id "+str(data['camera_id']) +" dengan transaksi : "+ str(data['trx_id'])+ "\n"
					count=count+1

	if len(inactive_transaction_lpr)==0 and "License Plate Recognition" in used_rule:
		missing_cam_lpr = ">>>>  Terdapat "+str(len(inactive_transaction_lpr))+" camera LPR tidak melakukan dumping"+"\n"
		missing_cam_detail_lpr= ""
	else:
		for data in inactive_transaction_lpr:
			if data["rule_name"] in used_rule:
				missing_cam_lpr = ">>>>  Terdapat "+str(len(inactive_transaction_lpr))+" camera LPR tidak melakukan dumping"+"\n"
				missing_cam_detail_lpr= ""
				count = 1
				for data in inactive_transaction_lpr:
					missing_cam_detail_lpr = missing_cam_detail_lpr  +str(count) + ". camera id "+str(data['camera_id']) +" dengan transaksi : "+ str(data['trx_id'])+ "\n"
					count=count+1

	if len(inactive_transaction_pc)==0 and "People Counting" in used_rule:
		missing_cam_pc = ">>>>  Terdapat "+str(len(inactive_transaction_pc))+" camera PEOPLE COUNTING tidak melakukan dumping"+"\n"
		missing_cam_detail_pc= ""
	else:
		for data in inactive_transaction_pc:
			if data["rule_name"] in used_rule:
				missing_cam_pc = ">>>>  Terdapat "+str(len(inactive_transaction_pc))+" camera PEOPLE COUNTING tidak melakukan dumping"+"\n"
				missing_cam_detail_pc= ""
				count = 1
				for data in inactive_transaction_pc:
					missing_cam_detail_pc = missing_cam_detail_pc  +str(count) + ". camera id "+str(data['camera_id']) +" dengan transaksi : "+ str(data['trx_id'])+ "\n"
					count=count+1

	if len(inactive_transaction_cb)==0 and "Crowd Behaviour" in used_rule:
		missing_cam_cb = ">>>>  Terdapat "+str(len(inactive_transaction_cb))+" camera CROWD BEHAVIOUR tidak melakukan dumping"+"\n"
		missing_cam_detail_cb= ""
	else:
		for data in inactive_transaction_cb:
			if data["rule_name"] in used_rule:
				missing_cam_cb = ">>>>  Terdapat "+str(len(inactive_transaction_cb))+" camera CROWD BEHAVIOUR tidak melakukan dumping"+"\n"
				missing_cam_detail_cb= ""
				count = 1
				for data in inactive_transaction_cb:
					missing_cam_detail_cb = missing_cam_detail_cb  +str(count) + ". camera id "+str(data['camera_id']) +" dengan transaksi : "+ str(data['trx_id'])+ "\n"
					count=count+1

	ending = "Terima Kasih"
	body = body + missing_cam_ce + missing_cam_detail_ce +missing_cam_fr_known+ missing_cam_detail_fr_known +missing_cam_fr_unknown+ missing_cam_detail_fr_unknown +missing_cam_vc+ missing_cam_detail_vc +missing_cam_lpr+ missing_cam_detail_lpr +missing_cam_pc+ missing_cam_detail_pc +missing_cam_cb+ missing_cam_detail_cb + ending
	return body
    
def send_email(username,password,recipient,message,date_report,hour_report):

    attachment = []
    msg = MIMEMultipart() 
    msg['From'] = username 
    msg['To'] = recipient
    msg['Subject'] = "Monitoring Report as "+str(date_report)+":"+str(hour_report)+".00 WIB"
    body = message
    msg.attach(MIMEText(body, 'plain'))
    text = msg.as_string()

    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(username, password)
        server.sendmail(username, recipient, text)
        server.close()
        print("successfully sent the mail")
    except:
        print("failed to send mail")

def cam_dumping_check_vc(conn, cur,date_comparison):
    cur.execute("SELECT camera_id FROM public.vehicle_counting_event where "+'"timestamp"'+" >= timestamp '"+date_comparison+"' group by camera_id order by camera_id asc")
    result = cur.fetchall()
    cam_list = []
    for data in result:
        cam_list.append(data[0])
    return cam_list

def cam_dumping_check_lpr(conn, cur,date_comparison):
    cur.execute("SELECT camera_id  FROM public.lpr_event where "+'"timestamp"'+" >= timestamp '"+date_comparison+"' group by camera_id order by camera_id asc")
    result = cur.fetchall()
    cam_list = []
    for data in result:
        cam_list.append(data[0])
    return cam_list

def cam_dumping_check_cb(conn, cur,date_comparison):
    cur.execute("SELECT camera_id  FROM public.crowd_behaviour_event where "+'"timestamp"'+" >= timestamp '"+date_comparison+"' group by camera_id order by camera_id asc")
    result = cur.fetchall()
    cam_list = []
    for data in result:
        cam_list.append(data[0])
    return cam_list

def cam_dumping_check_pc(conn, cur,date_comparison):
    cur.execute("SELECT camera_id  FROM public.people_counting_event where "+'"timestamp"'+" >= timestamp '"+date_comparison+"' group by camera_id order by camera_id asc")
    result = cur.fetchall()
    cam_list = []
    for data in result:
        cam_list.append(data[0])
    return cam_list

def cam_dumping_check_fr_known(conn, cur,date_comparison):
    cur.execute("SELECT camera_id  FROM public.fr_face_known_event where "+'"timestamp"'+" >= timestamp '"+date_comparison+"' group by camera_id order by camera_id asc")
    result = cur.fetchall()
    cam_list = []
    for data in result:
        cam_list.append(data[0])
    return cam_list

def cam_dumping_check_fr_unknown(conn, cur,date_comparison):
    cur.execute("SELECT camera_id FROM public.fr_face_unknown_event where "+'"timestamp"'+" >= timestamp '"+date_comparison+"' group by camera_id order by camera_id asc")
    result = cur.fetchall()
    cam_list = []
    for data in result:
        cam_list.append(data[0])
    return cam_list

def cam_dumping_check_ce(conn, cur,date_comparison):
    cur.execute("SELECT camera_id  FROM public.crowd_estimation_event where "+'"timestamp"'+" >= timestamp '"+date_comparison+"' group by camera_id order by camera_id asc")
    result = cur.fetchall()
    cam_list = []
    for data in result:
        cam_list.append(data[0])
    # print(cam_list)
    return cam_list

def get_rule_col(conn,cur):
	cur.execute("SELECT id, name  FROM public.rule_collection order by id asc")
	result = cur.fetchall()
	result_list = []
	for data in result:
		temp = dict()
		temp['rule_name'] = data[1]
		temp['rule_id'] = data[0]
		result_list.append(temp)
	return result_list

def get_cam_trx(conn, cur):
	cur.execute("SELECT public.transaction_camera_rule_vas_collection.camera_id, public.transaction_camera_rule_vas_collection.id, public.rule_collection.name FROM public.transaction_camera_rule_vas_collection INNER JOIN public.rule_collection on public.transaction_camera_rule_vas_collection.rule_id=public.rule_collection.id")
	result = cur.fetchall()
	result_list = []
	for data in result:
		temp = dict()
		temp['camera_id'] = data[0]
		temp['trx_id'] = data[1]
		temp['rule_name']=data[2]
		result_list.append(temp)
	return result_list

def get_last_dumping_vehicle(conn,cur,cam_id):
	cam_id=str(cam_id)
	cur.execute("SELECT public.vehicle_counting_event.timestamp FROM public.vehicle_counting_event WHERE public.vehicle_counting_event.camera_id= "+cam_id+" ORDER BY id DESC LIMIT 1")
	result = cur.fetchall()
	result_list = []
	for data in result:
		temp = dict()
		temp['timestamp'] = data[0]
		result_list.append(temp)
	return result_list

def get_last_dumping_crowd(conn,cur,cam_id):
	cam_id=str(cam_id)
	cur.execute("SELECT public.crowd_estimation_event.timestamp FROM public.crowd_estimation_event WHERE public.crowd_estimation_event.camera_id= "+cam_id+" ORDER BY id DESC LIMIT 1")
	result = cur.fetchall()
	result_list = []
	for data in result:
		temp = dict()
		temp['timestamp'] = data[0]
		result_list.append(temp)
	return result_list

def get_last_dumping_fr_known(conn,cur,cam_id):
	cam_id=str(cam_id)
	cur.execute("SELECT public.fr_face_known_event.timestamp FROM public.fr_face_known_event WHERE public.fr_face_known_event.camera_id= "+cam_id+" ORDER BY id DESC LIMIT 1")
	result = cur.fetchall()
	result_list = []
	for data in result:
		temp = dict()
		temp['timestamp'] = data[0]
		result_list.append(temp)
	return result_list

def get_last_dumping_fr_unknown(conn,cur,cam_id):
	cam_id=str(cam_id)
	cur.execute("SELECT public.fr_face_unknown_event.timestamp FROM public.fr_face_unknown_event WHERE public.fr_face_unknown_event.camera_id= "+cam_id+" ORDER BY id DESC LIMIT 1")
	result = cur.fetchall()
	result_list = []
	for data in result:
		temp = dict()
		temp['timestamp'] = data[0]
		result_list.append(temp)
	return result_list

def get_last_dumping_lpr(conn,cur,cam_id):
	cam_id=str(cam_id)
	cur.execute("SELECT public.lpr_event.timestamp FROM public.lpr_event WHERE public.lpr_event.camera_id= "+cam_id+" ORDER BY id DESC LIMIT 1")
	result = cur.fetchall()
	result_list = []
	for data in result:
		temp = dict()
		temp['timestamp'] = data[0]
		result_list.append(temp)
	return result_list

def get_last_dumping_pc(conn,cur,cam_id):
	cam_id=str(cam_id)
	cur.execute("SELECT public.people_counting_event.timestamp FROM public.people_counting_event WHERE public.people_counting_event.camera_id= "+cam_id+" ORDER BY id DESC LIMIT 1")
	result = cur.fetchall()
	result_list = []
	for data in result:
		temp = dict()
		temp['timestamp'] = data[0]
		result_list.append(temp)
	return result_list

def get_last_dumping_cb(conn,cur,cam_id):
	cam_id=str(cam_id)
	cur.execute("SELECT public.crowd_behaviour_event.timestamp FROM public.crowd_behaviour_event WHERE public.crowd_behaviour_event.camera_id= "+cam_id+" ORDER BY id DESC LIMIT 1")
	result = cur.fetchall()
	result_list = []
	for data in result:
		temp = dict()
		temp['timestamp'] = data[0]
		result_list.append(temp)
	return result_list

def do_checking():
	conn = None
	cur = None
	try:
		conn = psycopg2.connect( "dbname= "+ db_name+" user= "+db_username +"  host= "+ db_address+"  password= "+db_password )
	except:
		print("error connection please check connection config")
	cur = conn.cursor()
	current_time = datetime.now()
	time_range = current_time - timedelta(hours = 3)
	date_comparison = str(time_range.year)+'-'+str(time_range.month)+'-'+str(time_range.day)+' '+str(time_range.hour)+':'+str(time_range.minute)+':'+str(time_range.second)
	#date_comparison = '2019-02-28'
	date_check  = str(current_time.year)+'-'+str(current_time.month)+'-'+str(current_time.day)
	list_rule=get_cam_trx(conn,cur)

	crowdest_last_dump=[]
	fr_know_last_dump=[]
	fr_unknow_last_dump=[]
	lpr_last_dump=[]
	vc_last_dump=[]
	pc_last_dump=[]
	crowd_behaviour_last_dump=[]
	used_rule=[]

	crowdest_cam=[]
	fr_cam=[]
	lpr_cam=[]
	vc_cam=[]
	pc_cam=[]
	crowd_behaviour_cam=[]

	for i in list_rule:
		tmpce=dict()
		tmpfrknow=dict()
		tmpfrunknow=dict()
		tmplpr=dict()
		tmpvc=dict()
		tmppc=dict()
		tmpcb=dict()

		if i["rule_name"] not in used_rule:
			used_rule.append(i['rule_name'])

		if i["rule_name"]=="Crowd Estimation":
			tmpce['trx_id']=i['trx_id']
			tmpce['camera_id']=i['camera_id']
			tmpce['rule_name']=i['rule_name']
			crowdest_cam.append(i['camera_id'])
			dumpce=get_last_dumping_crowd(conn,cur,i["camera_id"])
			for x in dumpce:
				tmpce['last_dumping_ce']=format(x['timestamp'])
			crowdest_last_dump.append(tmpce)

		elif i["rule_name"]=="Face Recognition":
			tmpfrknow['trx_id']=i['trx_id']
			tmpfrknow['camera_id']=i['camera_id']
			tmpfrknow['rule_name']=i['rule_name']

			tmpfrunknow['trx_id']=i['trx_id']
			tmpfrunknow['camera_id']=i['camera_id']
			tmpfrunknow['rule_name']=i['rule_name']
			fr_cam.append(i['camera_id'])
			dumpunknow=get_last_dumping_fr_unknown(conn,cur,i["camera_id"])
			dumpknow=get_last_dumping_fr_known(conn,cur,i["camera_id"])
			for x in dumpknow:
				tmpfrknow['last_dumping_fr_known']=format(x['timestamp'])
			fr_know_last_dump.append(tmpfrknow)
			for x in dumpunknow:
				tmpfrunknow['last_dumping_fr_unknown']=format(x['timestamp'])
			fr_unknow_last_dump.append(tmpfrunknow)

		elif i["rule_name"]=="License Plate Recognition":
			tmplpr['trx_id']=i['trx_id']
			tmplpr['camera_id']=i['camera_id']
			tmplpr['rule_name']=i['rule_name']
			lpr_cam.append(i['camera_id'])
			dumplpr=get_last_dumping_lpr(conn,cur,i["camera_id"])
			for x in dumplpr:
				tmplpr['last_dumping_lpr']=format(x['timestamp'])
			lpr_last_dump.append(tmplpr)

		elif i["rule_name"]=="Vehicle Counting":
			tmpvc['trx_id']=i['trx_id']
			tmpvc['camera_id']=i['camera_id']
			tmpvc['rule_name']=i['rule_name']
			vc_cam.append(i['camera_id'])
			dumpvc=get_last_dumping_vehicle(conn,cur,i["camera_id"])
			for x in dumpvc:
				tmpvc['last_dumping_vc']=format(x['timestamp'])
			vc_last_dump.append(tmpvc)

		elif i["rule_name"]=="People Counting":
			tmppc['trx_id']=i['trx_id']
			tmppc['camera_id']=i['camera_id']
			tmppc['rule_name']=i['rule_name']
			dumppc=get_last_dumping_pc(conn,cur,i["camera_id"])
			pc_cam.append(i['camera_id'])
			for x in dumppc:
				tmppc['last_dumping_pc']=format(x['timestamp'])
			pc_last_dump.append(tmppc)

		elif i["rule_name"]=="Crowd Behaviour":
			tmpcb['trx_id']=i['trx_id']
			tmpcb['camera_id']=i['camera_id']
			tmpcb['rule_name']=i['rule_name']
			crowd_behaviour_cam.append(i['camera_id'])
			dumpcb=get_last_dumping_cb(conn,cur,i["camera_id"])
			for x in dumpcb:
				tmpcb['last_dumping_cb']=format(x['timestamp'])
			crowd_behaviour_last_dump.append(tmpcb)

	cam_list_dump_ce = cam_dumping_check_ce(conn,cur,date_comparison)
	cam_list_dump_fr_unknown = cam_dumping_check_fr_unknown(conn,cur,date_comparison)
	cam_list_dump_fr_known = cam_dumping_check_fr_known(conn,cur,date_comparison)
	cam_list_dump_vc = cam_dumping_check_vc(conn,cur,date_comparison)
	cam_list_dump_lpr = cam_dumping_check_lpr(conn,cur,date_comparison)
	cam_list_dump_pc = cam_dumping_check_pc(conn,cur,date_comparison)
	cam_list_dump_cb = cam_dumping_check_cb(conn,cur,date_comparison)
	conn.close()

	cam_diff_ce= inactive_camera_id(crowdest_cam,cam_list_dump_ce)
	inactive_transaction_ce= inactive_trx_id(cam_diff_ce,crowdest_last_dump)

	cam_diff_fr_unknown= inactive_camera_id(fr_cam,cam_list_dump_fr_unknown)
	inactive_transaction_fr_unknown= inactive_trx_id(cam_diff_fr_unknown,fr_unknow_last_dump)

	cam_diff_fr_known= inactive_camera_id(fr_cam,cam_list_dump_fr_known)
	inactive_transaction_fr_known= inactive_trx_id(cam_diff_fr_known,fr_know_last_dump)

	cam_diff_vc= inactive_camera_id(vc_cam,cam_list_dump_vc)
	inactive_transaction_vc= inactive_trx_id(cam_diff_vc,vc_last_dump)

	cam_diff_lpr= inactive_camera_id(lpr_cam,cam_list_dump_lpr)
	inactive_transaction_lpr= inactive_trx_id(cam_diff_lpr,lpr_last_dump)

	cam_diff_pc= inactive_camera_id(pc_cam,cam_list_dump_pc)
	inactive_transaction_pc= inactive_trx_id(cam_diff_pc,pc_last_dump)

	cam_diff_cb= inactive_camera_id(crowd_behaviour_cam,cam_list_dump_cb)
	inactive_transaction_cb= inactive_trx_id(cam_diff_cb,crowd_behaviour_last_dump)

	# for ur in used_rule:
	# 	if ur == "License Plate Recognition":
	# 		print(lpr_last_dump)
	# 		print (len(inactive_transaction_lpr))
	# 	elif ur == "Crowd Behaviour":
	# 		print(crowd_behaviour_last_dump)
	# 		print (len(inactive_transaction_cb))
	# 	elif ur == "People Counting":
	# 		print(pc_last_dump)
	# 		print(len(inactive_transaction_pc))
	# 	elif ur == "Face Recognition":
	# 		print(fr_know_last_dump)
	# 		print(len(inactive_transaction_fr_known))
	# 		print(fr_unknow_last_dump)
	# 		print(len(inactive_transaction_fr_unknown))
	# 	elif ur == "Crowd Estimation":
	# 		print(crowdest_last_dump)
	# 		print(len(inactive_transaction_ce))
	# 	elif ur == "Vehicle Counting":
	# 		print(vc_last_dump)
	# 		print(len(inactive_transaction_vc))

	current_time = datetime.now()
	message = create_body(used_rule,date_comparison,current_time.hour,inactive_transaction_ce,inactive_transaction_fr_known,inactive_transaction_fr_unknown,inactive_transaction_vc,inactive_transaction_lpr,inactive_transaction_pc,inactive_transaction_cb, date_check)
	send_email("rizkifika@nodeflux.io","rizkifika121","support@nodeflux.io",message,date_check,current_time.hour)
	# print(message)

if __name__ == '__main__':
	do_checking()
	# while True:
		# current_time = datetime.now()
		# if int(current_time.hour)%30==0:
		# 	do_checking()
		# time.sleep(3600)